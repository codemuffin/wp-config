<?php

// Environment DB Config: Development
// ============================================================================

define( 'WP_ENV',      'development' );

define( 'DB_NAME',     'DATABASE_NAME' );
define( 'DB_USER',     'USERNAME_HERE' );
define( 'DB_PASSWORD', 'PASSWORD_HERE' );
define( 'DB_HOST',     'HOST_IP_HERE' );

define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

$table_prefix  = 'wp_';


// Authentication: Salts & Keys
// ============================================================================

// Regenerate to log out all users: https://api.wordpress.org/secret-key/1.1/salt/
define('AUTH_KEY',         'unique_key');
define('SECURE_AUTH_KEY',  'unique_key');
define('LOGGED_IN_KEY',    'unique_key');
define('NONCE_KEY',        'unique_key');
define('AUTH_SALT',        'unique_salt');
define('SECURE_AUTH_SALT', 'unique_salt');
define('LOGGED_IN_SALT',   'unique_salt');
define('NONCE_SALT',       'unique_salt');


// Debug
// ============================================================================

// Enable debugging (default: false)
define( 'WP_DEBUG', true );

// Log debug messages to /wp-content/debug.log (false)
define( 'WP_DEBUG_LOG', true );

// Display debug messages (false)
define( 'WP_DEBUG_DISPLAY', true );

// Save queries for analysis. Useful with Query Monitor (false)
define( 'SAVEQUERIES', false );


// Admin
// ============================================================================

// Enable admin style & script concatenation (true)
define( 'CONCATENATE_SCRIPTS', false );

// Log admin script debug info to console (mostly useless) (false)
define( 'SCRIPT_DEBUG', false );


// Resctrictions
// ============================================================================

// Disable theme & plugin editor (false)
define( 'DISALLOW_FILE_EDIT', true );

// Disable theme & plugin management: editor, update, install (false)
define( 'DISALLOW_FILE_MODS', false );


// Automatic Updates
// ============================================================================

// Control core auto updates ('minor')
//   false    Disable minor & major
//   true     Enable  minor & major
//   'minor'  Enable  minor
define( 'WP_AUTO_UPDATE_CORE', 'minor' );

// Control all updates, minor and major (false)
define( 'AUTOMATIC_UPDATER_DISABLED', true );


// URLs
// ============================================================================

// Website URL and WP directory URL
define( 'WP_SITEURL', 'https://' . $_SERVER['HTTP_HOST'] . '/' );
define( 'WP_HOME',    'https://' . $_SERVER['HTTP_HOST'] . '/' );
