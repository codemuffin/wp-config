<?php

// Environment Domains
// ============================================================================

$wp_environments = array(

	// Development (Local & Staging)
	'development' => array(
		'TESTNAME.localdev',
		'TESTNAME.staginserver.com'
	),

	// Production
	'production' => array (
		'LIVENAME.com',
		'www.LIVENAME.com',
	)
);


// Environment Configs
// ============================================================================

if ( in_array( $_SERVER['SERVER_NAME'], $wp_environments['development'] ) )
{
	require_once( dirname(__FILE__) . '/wp-config-development.php' );
}
elseif( in_array( $_SERVER['SERVER_NAME'], $wp_environments['production']  ) )
{
	require_once( dirname(__FILE__) . '/wp-config-production.php' );
}
else
{
	// No default to prevent incorrect DB changes
	exit( 'Server name is not allowed: ' . $_SERVER['SERVER_NAME'] );
}


// WP
// ============================================================================

// Absolute path to root dir
!defined('ABSPATH') && define('ABSPATH', dirname(__FILE__) . '/');

// Set up WP vars & includes
require_once(ABSPATH . 'wp-settings.php');
